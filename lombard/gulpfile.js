var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('concat-js', function () {
    gulp.src([ './bower_components/jquery/dist/jquery.js'
             , './bower_components/underscore/underscore.js'
             , './bower_components/angular/angular.js'
             , './bower_components/angular-animate/angular-animate.js'
             , './bower_components/angular-aria/angular-aria.js'
             , './bower_components/angular-messages/angular-messages.js'
             , './bower_components/angular-material/angular-material.js'
             , './bower_components/angular-route/angular-route.js'
             , './bower_components/angular-resource/angular-resource.js'


             , './application/js/app.js'
             , './application/js/**/*.js' ])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./application/dist'))
});

gulp.task('watch', function () {
    gulp.watch(['./application/js/**/*.js'], ['default']);
});

gulp.task('default', ['concat-js']);