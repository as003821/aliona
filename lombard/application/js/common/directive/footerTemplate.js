(function(){
    angular
        .module('app')
        .directive('footerTemplate',footerTemplate);
    function footerTemplate(){
        return{
            restrict:"E",
            templateUrl:"application/js/common/template/footer-template.html"
        };
    }
}());