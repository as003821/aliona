(function(){
    angular
        .module('app',['ngRoute', 'ngResource'])
		 .config(function ($routeProvider) {

            $routeProvider
                .when('/signin', {
                    templateUrl: 'application/js/routes/auth/index.html',
                })
                .when('/registr', {
                    templateUrl: 'application/js/routes/registration/registration.html',

                })
                .when('/', {
                    templateUrl: 'application/js/routes/main/index.html',
                    
                })
                .when('/confirmation',{
                    templateUrl:'application/js/routes/main/index.html',
                })
        })
        .controller('appCtrl', AppCtrl);
    function AppCtrl(){
    }

}());

