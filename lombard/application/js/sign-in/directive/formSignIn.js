(function(){
    angular
        .module('app')
        .directive("formSignIn", formSignIn);
    function formSignIn(){
        return {
            restrict:"E",
            templateUrl:"application/js/sign-in/template/sign-in-template.html",
            controller: signInController //"appCtrl"
        };
    }
    signInController.$inject = ['$scope', '$http'];
    function signInController ($scope, $http) {
        $scope.user = {};
        $scope.saveData = function() {
            console.log($scope.user);
            $http.post("http://lombard.dev.orangesoft.co/api/v1/auth/users/sign_in", $scope.user)
                .then(function(response) {
                    $scope.content = response.data;
                    $scope.statuscode = response.status;
                    $scope.statustext = response.statustext;
                    console.log( $scope.content.user);
                });
        };
    }
}());